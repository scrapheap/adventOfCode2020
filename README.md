# Advent Of Code 2020

These are my solutions to the [Advent Of Code 2020](https://adventofcode.com/2020)
challenges.

They're in Perl and you'll need to use cpanminus to install the dependencies (
if you have Make installed as well you can probably just run
`make installDependencies` to install them).

Test can be found in the `t` directory (run them with `make test`)

Scripts to solve each day's challenges can be found in the `bin` directory.

The actually modules that do most of the work are locate in the `lib`
directory.
