requires 'Modern::Perl';
requires 'Readonly';
requires 'Test::Deep';
requires 'Try::Tiny';
requires 'Test::Exception';
requires 'Math::Utils';
requires 'Math::ModInt';
requires 'Math::ModInt::ChineseRemainder';
