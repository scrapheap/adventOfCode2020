CPANM=cpanm
CPAN_LIB=cpan

PROVE=prove
PROVE_OPTIONS=-I./cpan/lib/perl5 -I./lib

.PHONY: all
all: test

.PHONY: test
test:
	$(PROVE) $(PROVE_OPTIONS)

installDependencies:
	mkdir -p $(CPAN_LIB)
	$(CPANM) -L $(CPAN_LIB) --installdeps .
