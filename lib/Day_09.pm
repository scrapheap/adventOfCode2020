package Day_09;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( isNotSumOfTwoOfThePrevious rangesThatAddUpTo );

use List::Util qw( sum );

use Day_01 qw( findTwoEntriesThatSumTo );


sub isNotSumOfTwoOfThePrevious {
    my ( $windowSize, @list ) = @_;

    my @notSumOfTwoOfThePrevious;

    foreach my $windowEnd ( $windowSize .. $#list ) {
        my @window = @list[($windowEnd - $windowSize) .. ( $windowEnd - 1 )];
        my $targetValue = $list[ $windowEnd ];

        push @notSumOfTwoOfThePrevious, $targetValue  unless  findTwoEntriesThatSumTo( $targetValue, @window );
    }

    return @notSumOfTwoOfThePrevious;
};


sub rangesThatAddUpTo {
    my ( $target, @list ) = @_;

    my @ranges;

    my $windowStart = 0;
    my $windowEnd = 0;

    while ( $windowStart < @list ) {
        my @window = @list[$windowStart .. $windowEnd];
        my $windowTotal = sum( @window );

        push @ranges, [ @window ]  if  $windowTotal == $target  &&  @window >= 2;

        if ( $windowEnd != $#list  &&  ( $windowStart == $windowEnd  || $windowTotal < $target ) ) {
            $windowEnd++;
        } else {
            $windowStart++;
        }
    }

    return @ranges;
}

1;
