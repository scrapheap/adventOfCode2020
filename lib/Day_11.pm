package Day_11;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( nextOccupancy nextOccupancy2 occupied );

use List::Util qw( max reduce sum );

sub nextOccupancy {
    my @seats = @_;

    my %seatingRules = (
        '.' => sub { '.' },
        'L' => sub { occupied( @_ ) ? 'L' : '#' },
        '#' => sub { occupied( @_ ) >= 4 ? 'L': '#' },
    );

    my $height = $#seats;
    my $width = $#{ $seats[0] };

    my @nextLayout;

    foreach my $x ( 0 .. $height ) {
        foreach my $y ( 0 .. $width ) {
            $nextLayout[$x][$y] = newSeatingState( \%seatingRules, \&immediateNeighbours, $x, $y, @seats);
        }
    }

    return @nextLayout;
}


sub newSeatingState {
    my ( $seatingRules, $neighbours, $x, $y, @seats ) = @_;

    return $seatingRules->{ $seats[ $x ][ $y ] }->( $neighbours->( $x, $y, @seats ) );
}


sub occupied {
    return grep { /#/ } map { ref $_ eq "ARRAY" ? @$_ : $_ } @_;
}


sub immediateNeighbours {
    my ( $x, $y, @seats ) = @_;

    my @rows = grep { $_ >= 0 && $_ < @seats } ( $x - 1, $x, $x + 1 );
    my @columns = grep { $_ >= 0 && $_ < @{ $seats[0] } } ( $y - 1, $y, $y + 1 );

    my @neighbours;

    foreach my $row ( @rows ) {
        CELL:
        foreach my $col ( @columns ) {
            next CELL if $x == $row && $y == $col;

            push @neighbours, $seats[ $row ][ $col ];
        }
    }

    return @neighbours;
}


sub lineOfSightNeighbours {
    my ( $x, $y, @seats ) = @_;

    my @neighbours;

    my $maxX = $#seats;
    my $maxY = $#{ $seats[0] };

    my %directions = (
        upLeft    => sub { my ( $x, $y ) = @_;  return ( $x - 1, $y - 1 ) },
        up        => sub { my ( $x, $y ) = @_;  return ( $x - 1, $y     ) },
        upRight   => sub { my ( $x, $y ) = @_;  return ( $x - 1, $y + 1 ) },
        left      => sub { my ( $x, $y ) = @_;  return ( $x    , $y - 1 ) },
        right     => sub { my ( $x, $y ) = @_;  return ( $x    , $y + 1 ) },
        downLeft  => sub { my ( $x, $y ) = @_;  return ( $x + 1, $y - 1 ) },
        down      => sub { my ( $x, $y ) = @_;  return ( $x + 1, $y     ) },
        downRight => sub { my ( $x, $y ) = @_;  return ( $x + 1, $y + 1 ) },
    );

    DIRECTION:
    foreach my $move ( values %directions ) {
        my ( $_x, $_y ) = $move->( $x, $y );

        while ( $_x >= 0 && $_x <= $maxX && $_y >= 0 && $_y <= $maxY ) {
            if ( $seats[$_x][$_y] =~ m/L|#/ ) {
                push @neighbours, $seats[$_x][$_y];
                next DIRECTION;
            }

            ( $_x, $_y ) = $move->( $_x, $_y );
        }
    }

    return @neighbours;
}


sub nextOccupancy2 {
    my @seats = @_;

    my %seatingRules = (
        '.' => sub { '.' },
        'L' => sub { occupied( @_ ) ? 'L' : '#' },
        '#' => sub { occupied( @_ ) >= 5 ? 'L': '#' },
    );

    my $height = $#seats;
    my $width = $#{ $seats[0] };

    my @nextLayout;

    foreach my $x ( 0 .. $height ) {
        foreach my $y ( 0 .. $width ) {
            $nextLayout[$x][$y] = newSeatingState( \%seatingRules, \&lineOfSightNeighbours, $x, $y, @seats);
        }
    }

    return @nextLayout;
}

1;
