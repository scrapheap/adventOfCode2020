package Day_04;

use Modern::Perl;
use Exporter qw( import );

use List::Util qw( reduce );
use Readonly;

our @EXPORT_OK = qw( recordsInBatch validRecord );

sub recordsInBatch {
    my ( $batch ) = @_;

    my @records = split /\r?\n\r?\n/, $batch;

    return map { { stringToRecord( $_ ) } }  @records;
};


sub stringToRecord {
    my ( $string ) = @_;

    return split /:|\s+/s, $string;
}


Readonly our %SUB_RULE => (
    cm  => sub {  $_[0] >= 150  &&  $_[0] <= 193  },
    in  => sub {  $_[0] >= 59   &&  $_[0] <= 76  },
);

Readonly our %VALIDATION_RULE => (
    byr => sub {  $_[0] =~ m/^\d{4}$/s  &&  $_[0] >= 1920  &&  $_[0] <= 2002  },
    iyr => sub {  $_[0] =~ m/^\d{4}$/s  &&  $_[0] >= 2010  &&  $_[0] <= 2020  },
    eyr => sub {  $_[0] =~ m/^\d{4}$/s  &&  $_[0] >= 2020  &&  $_[0] <= 2030  },

    hgt => sub {  $_[0] =~ m/^(\d+)(cm|in)$/s  &&  $SUB_RULE{ $2 }->( $1 )  },
    
    hcl => sub {  $_[0] =~ m/^#[0-9abcdef]{6}$/s  },

    ecl => sub {  $_[0] =~ m/^(?:amb|blu|brn|gry|grn|hzl|oth)$/s  },

    pid => sub {  $_[0] =~ m/^\d{9}$/s  },
);

sub validRecord {
    my ( $record ) = @_;

    return reduce { $a && exists $record->{ $b } && $VALIDATION_RULE{ $b }->( $record->{ $b } ) }  1, keys %VALIDATION_RULE;
}

1;
