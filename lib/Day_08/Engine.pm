package Day_08::Engine;

use Modern::Perl;

sub new {
    my ( $class, %parameters ) = @_;

    return bless {}, $class;
};


sub load {
    my ( $self, @program ) = @_;

    $self->{program} = [ _parseProgram( @program ) ];

    return $self;
};


sub run {
    my ( $self ) = @_;

    my %state = (
        PC => 0,
        accumulator => 0,
    );

    my %seenBefore;
    
    while ( $self->{program}->[ $state{PC} ] ) {
        die "Infinite loop - accumulator: $state{accumulator}" if $seenBefore{ $state{PC} };

        $seenBefore{ $state{PC} } = 1;
        %state = $self->{program}->[ $state{PC} ]->( %state );
        $state{PC}++;
    }

    return $state{accumulator};
}


sub fixProgram {
    my ( $self, @program ) = @_;

    my %swaps = ( jmp => 'nop', nop => 'jmp' );

    ATTEMPT:
    foreach my $line ( 0..$#program ) {
        next ATTEMPT unless $program[$line] =~ m/^(?:jmp|nop)/;

        my @attemptedFix = @program;
        $attemptedFix[ $line ] =~ s/^(jmp|nop)/$swaps{$1}/;

        $self->load( @attemptedFix );

        eval { $self->run() };

        return @attemptedFix unless $@;
    }

    die "No fixes found";
}


sub _parseProgram {
    return map { _parseLine( $_ ) } @_;
}


my %operations = (
    nop => sub {
        return sub { return @_ }
    },
    acc => sub {
        my ( $operand ) = @_;

        return sub {
            my %state = @_;
            
            $state{accumulator} += $operand;

            return %state;
        }
    },
    jmp => sub {
        my ( $operand ) = @_;

        $operand--;

        return sub {
            my %state = @_;

            $state{PC} += $operand;

            return %state;
        }
    },
);


sub _parseLine {
    my ( $line ) = @_;

    my ( $opcode, $operand ) = $line =~ m/^(nop|acc|jmp)\s+([-+]?\d+)/;

    return $operations{ $opcode }->( $operand );
};

1;
