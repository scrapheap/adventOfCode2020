package Day_02;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( validatePasswordAgainstPolicy validatePasswordAgainstTobogganPolicy );

sub validatePasswordAgainstPolicy {
    my ( $policy, $password ) = @_;

    my ( $min, $max, $char ) = $policy =~ m/^(\d+)\-(\d+)\s+(\w+)/;

    my $count = grep { $_ eq $char } split //, $password;

    return $min <= $count && $count <= $max;
}

sub validatePasswordAgainstTobogganPolicy {
    my ( $policy, $password ) = @_;

    my ( $indexA, $indexB, $char ) = $policy =~ m/^(\d+)\-(\d+)\s+(\w+)/;

    my @password = ( undef, split //, $password );

    return $password[$indexA] ne $password[$indexB] && ( $password[$indexA] eq $char || $password[$indexB] eq $char );
}


1;
