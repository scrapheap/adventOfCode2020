package Day_05;

use Modern::Perl;
use Exporter qw( import );

use List::Util qw( reduce );
use Readonly;

our @EXPORT_OK = qw( seatIdFromBoardingPass missingIds );


sub seatIdFromBoardingPass {
    my ( $boardingPass ) = @_;

    my $binary = $boardingPass =~ tr/FBLR/0101/r;

    return oct( "0b$binary" );
}


sub missingIds {
    my @seatIds = @_;

    my @missingIds;

    reduce {
        if ( $b - $a == 2 ) {
          push @missingIds, $a + 1
        };

        $b
    } sort { $a <=> $b } @seatIds;


    return @missingIds;
}

1;
