package Day_13;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( nearestBus nextPickupIn startOfOrderedSequence );

use List::Util qw( reduce );
use Math::ModInt qw( mod );
use Math::ModInt::ChineseRemainder qw( cr_combine );


sub nearestBus {
    my ( $time, @schedule ) = grep { /^\d+$/ } @_;

    return reduce { nextPickupIn( $time, $a ) < nextPickupIn( $time, $b )  ?  $a  :  $b } @schedule;
}

sub nextPickupIn {
    my ( $time, $bus ) = @_;

    return $time % $bus ? $bus - $time % $bus : 0;
}


sub startOfOrderedSequence {
    my @busses = @_;
    my @mods = map { $busses[ $_ ] ne 'x'  ?  mod( $_, $busses[ $_ ] )  :  () } ( 0 .. $#_ );

    my $chineseRemainder = cr_combine( @mods );

    return $chineseRemainder->modulus - $chineseRemainder->residue;
}


1;
