package Day_07;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( parseBaggageRules bagsThatContain totalBags );

use List::Util qw( any sum );


sub parseBaggageRules {
    my %baggageRules;

    foreach my $rule ( @_ ) {
        my ( $bag, $contents ) = $rule =~ m/^(.*?) bags contain (.*)$/;

        $baggageRules{ $bag } =  { parseBagContents( $contents ) };
    };

    return %baggageRules;
}


sub parseBagContents {
    my ( $contents ) = @_;

    return map {
        my ($count, $bag) = $_ =~ m/(\d+) (.*?) bag/;

        $count ? ( $bag => $count ) : ();
    } split /, /, $contents;
};


sub bagsThatContain {
    my ( $bag, %baggageRules ) = @_;

    return grep { canContain( $bag, $_, %baggageRules ) } keys %baggageRules;
}


sub canContain {
    my ( $targetBag, $searchingBag, %baggageRules ) = @_;

    my @bagsContained = keys %{ $baggageRules{ $searchingBag } };

    return 0 unless @bagsContained;

    return 1 if grep { $_ eq $targetBag } @bagsContained;

    return any{ canContain( $targetBag, $_, %baggageRules ) }  keys %{ $baggageRules{ $searchingBag } };
}


sub totalBags {
    my ( $bag, %baggageRules ) = @_;

    my @bagContents = keys %{ $baggageRules{ $bag } };

    return sum(
        1,
        map {
            totalBags( $_, %baggageRules ) * $baggageRules{ $bag }->{ $_ }
        } @bagContents
    );
};


1;
