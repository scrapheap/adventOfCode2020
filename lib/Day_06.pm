package Day_06;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( uniqueAnswers unanimousAnswers );

use List::Util qw( uniq reduce );

sub uniqueAnswers {
    my @groups = @_;

    return map { [ uniq( grep { /\w/ } split //s ) ] } @groups;
}


sub unanimousAnswers {
    my @groups = @_;

    my @groupAnswers;
    foreach my $group (@groups) {
        my $memberCount = memberCount( $group );

        my $answerCounts = reduce { $a->{$b}++; $a } {}, sort grep { /\w/ } split //s, $group;

        push @groupAnswers, [ grep { $answerCounts->{ $_ } == $memberCount } sort keys %$answerCounts ];

    }

    return @groupAnswers;
}

sub memberCount {
    my ( $group ) = @_;

    return scalar split /[\r\n]+/, $group;
}

1;
