package Day_14;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( mask initialiseMemory locationMask initialiseMemory2 );

use List::Util qw( reduce );

sub mask {
    my ( $mask ) = @_;

    no warnings 'portable';

    my $drop = oct "0b" . $mask =~ tr/X/1/r;
    my $set  = oct "0b" . $mask =~ tr/X/0/r;

    return sub { ( $_[0] & $drop ) | $set };
}


sub initialiseMemory {
    my @data = map { [ split /\s*=\s*/, $_ ] } @_;

    my $mask;

    my $memory = reduce {
        if ( $b->[0] eq 'mask' ) {
            $mask = mask( $b->[1] );
        } else {
            my ( $loc ) = $b->[0] =~ m/(\d+)/;
            $a->[ $loc ] = $mask->( $b->[1] );
        }

        $a
    } [], @data;

    return @$memory;
}


sub locationMask {
    my ( $mask ) = @_;

    no warnings 'portable';

    my $fixedMask = oct "0b" . ( $mask =~ tr/X/0/r );
    my @masks = binaryMasks( enumerateMasks( split //, $mask ) );

    return sub {
        my ( $location ) = @_;

        $location |= $fixedMask;

        return map {
            ( $location & $_->{and} ) | $_->{or},
        } @masks;
    };
}

sub binaryMasks {
    no warnings 'portable';

    return map {  { and => oct("0b$_->{and}"), or => oct("0b$_->{or}") }  } @_;
}

sub enumerateMasks {
    my ( @maskBits ) = @_;

    return ( { and => '', or => '' } ) unless @maskBits;

    my $bit = shift @maskBits;

    return map {
        $bit eq 'X'
            ? ( _off( $_ ),  _on( $_ ) )
            : ( _keep( $_ ) )
    } enumerateMasks( @maskBits );
}

sub _off {
    my ( $mask ) = @_;
    return { and => "0$mask->{and}", or => "0$mask->{or}" };
}

sub _on {
    my ( $mask ) = @_;
    return { and => "1$mask->{and}", or => "1$mask->{or}" };
}

sub _keep {
    my ( $mask ) = @_;
    return { and => "1$mask->{and}", or => "0$mask->{or}" };
}


sub initialiseMemory2 {
    my @data = map { [ split /\s*=\s*/, $_ ] } @_;

    my $mask;

    my $memory = reduce {
        if ( $b->[0] eq 'mask' ) {
            $mask = locationMask( $b->[1] );
        } else {
            my ( $loc ) = $b->[0] =~ m/(\d+)/;

            foreach my $maskedLoc ( $mask->( $loc ) ) {
                $a->{ $maskedLoc } = $b->[1];
            }
        }

        $a
    } {}, @data;

    return %$memory;
}

1;
