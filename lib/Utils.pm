package Utils;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( trimAll gridFromRows );

sub trimAll {
    return map { s/^\s*(.*?)\s*[\r\n]$/$1/r } @_;
}

sub gridFromRows {
    return map { [ split //, $_ ] } @_;
}

1;
