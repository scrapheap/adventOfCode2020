package Day_03;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( countTreesInPath );

sub countTreesInPath {
    my ( $right, $down, @map ) = @_;

    my $mapWidth = scalar @{ $map[0] };

    my $count = 0;
    my $x = 0;
    my $y = 0;

    while ( $y < scalar @map ) {
        if( $map[$y][$x] eq '#' ) {
            $count++;
        }

        $x += $right;
        $x %= $mapWidth;
        $y += $down;
    };

    return $count;
};

1;
