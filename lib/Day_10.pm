package Day_10;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( deviceJoltLevel joltingDifferences joltHistogram buildTree walkTree );

use List::Util qw( max reduce sum );

use Memoize;

memoize( 'walkTree', NORMALIZER => sub { join ',', sort @_ } );


sub deviceJoltLevel {
    return max( @_ ) + 3;
}


sub joltingDifferences {
    my @joltLevels = sort { $a <=> $b } @_;
    my @differences;

    reduce { push @differences, ($b - $a);  $b } @joltLevels;

    return @differences;
}


sub joltHistogram {
    my $histogram = reduce { $a->{$b}++; $a } {}, @_;
    
    return %$histogram;
}


sub buildTree {
    my @adapters = sort { $b <=> $a } @_;

    my %tree;

    while ( my $adapter = shift @adapters ) {
        $tree{$adapter} = [ grep { $adapter - $_ <= 3 } @adapters ];
    }

    return %tree;
}


sub walkTree {
    my ( $node, %tree ) = @_;

    return 1 if $node == 0;

    return sum ( map { walkTree( $_, %tree ) } @{ $tree{ $node } } );
}

1;
