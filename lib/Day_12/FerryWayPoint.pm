package Day_12::FerryWayPoint;

use Modern::Perl;

sub new {
    my ( $class ) = @_;

    return bless { x => 0, y => 0, w => { x => 10, y => 1 } };
}

sub position {
    my ( $self ) = @_;

    return ( x => $self->{x}, y => $self->{y} );
}


my %moves;
%moves = (
    F => sub {
        my ( $self, $distance ) = @_;
        
        $self->{x} += $self->{w}->{x} * $distance;
        $self->{y} += $self->{w}->{y} * $distance;
        
        return $self
    },

    N => sub {
        my ( $self, $distance ) = @_;
       
        $self->{w}->{y} += $distance;

        return $self
    },
      
    E => sub {
        my ( $self, $distance ) = @_;
        
        $self->{w}->{x} += $distance;

        return $self
    },
      
    S => sub {
        my ( $self, $distance ) = @_;
        
        $self->{w}->{y} -= $distance;

        return $self
    },
      
    W => sub {
        my ( $self, $distance ) = @_;
        
        $self->{w}->{x} -= $distance;

        return $self
    },
      

    L => sub {
        my ( $self, $degrees ) = @_;
        
        $self->{w} = _rol( $self->{w}, $degrees );

        return $self
    },
      
    R => sub {
        my ( $self, $degrees ) = @_;
        
        $self->{w} = _rol( $self->{w}, 360 - $degrees );

        return $self
    },
      
);


sub _rol {
    my ( $w, $a ) = @_;

    while ( $a > 0 ) {
        $a -= 90;
        ( $w->{x}, $w->{y} ) = (  - $w->{y},  $w->{x} );
    }

    return $w;
}

sub move {
    my ( $self, $instruction ) = @_;

    my ( $direction, $distance ) = $instruction =~ m/^(\w)(\d+)/;

    ( $self ) = $moves{ $direction }->( $self, $distance );

    return $self;
}

1;
