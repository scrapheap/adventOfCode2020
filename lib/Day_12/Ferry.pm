package Day_12::Ferry;

use Modern::Perl;

sub new {
    my ( $class ) = @_;

    return bless { x => 0, y => 0, d => 'E' };
}

sub position {
    my ( $self ) = @_;

    return ( x => $self->{x}, y => $self->{y} );
}


my %moves;
%moves = (
    F => sub { $moves{ $_[2] }->( @_ ) },

    N => sub { ( $_[0]        ,  $_[1] + $_[3],  $_[2] ) },
    E => sub { ( $_[0] + $_[3],  $_[1]        ,  $_[2] ) },
    S => sub { ( $_[0]        ,  $_[1] - $_[3],  $_[2] ) },
    W => sub { ( $_[0] - $_[3],  $_[1]        ,  $_[2] ) },

    L => sub { ( $_[0]        ,  $_[1]        ,  _rol( $_[2], $_[3] ) ) },
    R => sub { ( $_[0]        ,  $_[1]        ,  _rol( $_[2], 360 - $_[3] ) ) },
);


sub _rol {
    my ( $d, $a ) = @_;

    my %next = ( N => 'W', W => 'S', S => 'E', E => 'N' );

    while ( $a > 0 ) {
        $a -= 90;
        $d = $next{ $d };
    }

    return $d;
}

sub move {
    my ( $self, $instruction ) = @_;

    my ( $direction, $distance ) = $instruction =~ m/^(\w)(\d+)/;

    ( $self->{x}, $self->{y}, $self->{d} ) = $moves{ $direction }->( $self->{x}, $self->{y}, $self->{d}, $distance );

    return $self;
}

1;
