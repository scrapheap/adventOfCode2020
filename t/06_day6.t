use Modern::Perl;

use Test::More;
use Test::Deep;

use Day_06 qw( uniqueAnswers unanimousAnswers );

my @groups = (
    "abc",
    "a\nb\nc",
    "ab\nac",
    "a\na\na\na",
    "b",
);


subtest 'Part 1' => sub {
    cmp_deeply(
        [ uniqueAnswers( @groups ) ],
        [
            [ 'a', 'b', 'c' ],
            [ 'a', 'b', 'c' ],
            [ 'a', 'b', 'c' ],
            [ 'a' ],
            [ 'b' ],
        ],
        "uniqueAnswers should return array refs of the unique answers for each group"
    );
};


subtest 'Part 2' => sub {
    cmp_deeply(
        [ unanimousAnswers( @groups ) ],
        [
            [ 'a', 'b', 'c' ],
            [ ],
            [ 'a' ],
            [ 'a' ],
            [ 'b' ],
        ],
        "allAnswered should return array refs of only those answers that were consistent in the group"
    );
};

done_testing();
