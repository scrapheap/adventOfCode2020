use Modern::Perl;

use Test::More;
use Test::Exception;
use Test::Deep;

use Day_08::Engine;

my @program = (
    "nop +0\n",
    "acc +1\n",
    "jmp +4\n",
    "acc +3\n",
    "jmp -3\n",
    "acc -99\n",
    "acc +1\n",
    "jmp -4\n",
    "acc +6\n",
);


subtest 'Part 1' => sub {
    my $engine = Day_08::Engine->new();

    ok( $engine->load( @program ), "Our engine should load our program" );

    throws_ok { $engine->run(); } qr/Infinite loop - accumulator: 5/, "Running the program should throw an infinite loop error";
};


subtest 'Part 2' => sub { 
    my $engine = Day_08::Engine->new();

    cmp_deeply(
        [ $engine->fixProgram( @program ) ],
        [
            "nop +0\n",
            "acc +1\n",
            "jmp +4\n",
            "acc +3\n",
            "jmp -3\n",
            "acc -99\n",
            "acc +1\n",
            "nop -4\n",
            "acc +6\n",
        ],
        "Fixing our broken program should swap out the penultimate line's jmp for a nop"
    );
};

done_testing();
