use Modern::Perl;

use Test::More;
use Test::Deep;

use Day_04 qw( recordsInBatch validRecord );

my $batch = <<EOB;
ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in
EOB

my @expectedRecords = (
    { byr => 1937,  cid => 147, ecl => 'gry',  eyr => 2020,  hcl => '#fffffd',  hgt => '183cm',  iyr => 2017,  pid => 860033327 },
    { byr => 1929,  cid => 350, ecl => 'amb',  eyr => 2023,  hcl => '#cfa07d',                   iyr => 2013,  pid => '028048884' },
    { byr => 1931,              ecl => 'brn',  eyr => 2024,  hcl => '#ae17e1',  hgt => '179cm',  iyr => 2013,  pid => 760753108 },
    {                           ecl => 'brn',  eyr => 2025,  hcl => '#cfa07d',  hgt => '59in',   iyr => 2011,  pid => 166559648 },
);


subtest 'Part 1' => sub {
    cmp_deeply(
        [ recordsInBatch( $batch ) ],
        \@expectedRecords,
        "The records extracted from the batch should be those we expect"
    );

    ok( validRecord( $expectedRecords[0] ), "A valid records should pass" );
    ok( ! validRecord( $expectedRecords[1] ), "Missing hgt field is invalid" );
    ok( validRecord( $expectedRecords[2] ), "Treat a missing cid field as valid" );
    ok( ! validRecord( $expectedRecords[3] ), "Missing byr field is invalid" );
};


subtest 'Part 2' => sub {
    subtest 'Validation Rules' => sub {
        ok( $Day_04::VALIDATION_RULE{byr}->( 1920 ), "A birth year of 1920 is valid" );
        ok( $Day_04::VALIDATION_RULE{byr}->( 2002 ), "A birth year of 2002 is valid" );
        ok( ! $Day_04::VALIDATION_RULE{byr}->( 1919 ), "A birth year lower than 1920 is invalid" );
        ok( ! $Day_04::VALIDATION_RULE{byr}->( 2003 ), "A birth year higher than 2002 is invalid" );
        ok( ! $Day_04::VALIDATION_RULE{byr}->( '2000.00' ), "A birth year of more than 4 digits is invalid");

        ok( $Day_04::VALIDATION_RULE{iyr}->( 2010 ), "An issue year of 2010 is valid" );
        ok( $Day_04::VALIDATION_RULE{iyr}->( 2020 ), "An issue year of 2020 is valid" );
        ok( ! $Day_04::VALIDATION_RULE{iyr}->( 2009 ), "An issue year lower than 2010 is invalid" );
        ok( ! $Day_04::VALIDATION_RULE{iyr}->( 2021 ), "An issue year higher than 2020 is invalid" );
        ok( ! $Day_04::VALIDATION_RULE{iyr}->( '2000.00' ), "An issue year of more than 4 digits is invalid");

        ok( $Day_04::VALIDATION_RULE{eyr}->( 2020 ), "An expiry year of 2020 is valid" );
        ok( $Day_04::VALIDATION_RULE{eyr}->( 2020 ), "An expiry year of 2030 is valid" );
        ok( ! $Day_04::VALIDATION_RULE{eyr}->( 2019 ), "An expiry year lower than 2020 is invalid" );
        ok( ! $Day_04::VALIDATION_RULE{eyr}->( 2031 ), "An expiry year higher than 2030 is invalid" );
        ok( ! $Day_04::VALIDATION_RULE{eyr}->( '2000.00' ), "An expiry year of more than 4 digits is invalid");

        ok( $Day_04::SUB_RULE{cm}->( '150' ), "An height of 150cm is valid" );
        ok( $Day_04::SUB_RULE{cm}->( '193' ), "An height of 193cm is valid" );
        ok( ! $Day_04::SUB_RULE{cm}->( '149' ), "An height lower than 150cm is invalid" );
        ok( ! $Day_04::SUB_RULE{cm}->( '194' ), "An height higher than 193cm is invalid" );

        ok( $Day_04::SUB_RULE{in}->( '59' ), "An height of 59in is valid" );
        ok( $Day_04::SUB_RULE{in}->( '76' ), "An height of 76in is valid" );
        ok( ! $Day_04::SUB_RULE{in}->( '58' ), "An height lower than 59in is invalid" );
        ok( ! $Day_04::SUB_RULE{in}->( '77' ), "An height higher than 77in is invalid" );

        ok( $Day_04::VALIDATION_RULE{hgt}->( '150cm' ), "A height of 150cm is valid" );
        ok( $Day_04::VALIDATION_RULE{hgt}->( '193cm' ), "A height of 193cm is valid" );
        ok( $Day_04::VALIDATION_RULE{hgt}->( '59in' ), "A height of 59in is valid" );
        ok( $Day_04::VALIDATION_RULE{hgt}->( '76in' ), "A height of 76in is valid" );
        ok( ! $Day_04::VALIDATION_RULE{hgt}->( '149cm' ), "A height lower than 150cm is invalid" );
        ok( ! $Day_04::VALIDATION_RULE{hgt}->( '194cm' ), "A height higher than 193cm is invalid" );
        ok( ! $Day_04::VALIDATION_RULE{hgt}->( '58in' ), "A height lower than 59in is invalid" );
        ok( ! $Day_04::VALIDATION_RULE{hgt}->( '77in' ), "A height higher than 77in is invalid" );
        ok( ! $Day_04::VALIDATION_RULE{hgt}->( '150pp' ), "A height with an unknown unit is invalid" );
        ok( ! $Day_04::VALIDATION_RULE{hgt}->( 'cm' ), "A height without a number is invalid" );

        ok( $Day_04::VALIDATION_RULE{hcl}->( '#123abc' ), "A valid hex colour code is valid" );
        ok( ! $Day_04::VALIDATION_RULE{hcl}->( '#123abz' ), "An invalid hex colour code is invalid" );
        ok( ! $Day_04::VALIDATION_RULE{hcl}->( '123abc' ), "A missing # is invalid" );

        foreach my $validColour ( qw( amb blu brn gry grn hzl oth ) ) {
            ok( $Day_04::VALIDATION_RULE{ecl}->( $validColour ), "An eye colour of $validColour is valid" );
        }
        ok( ! $Day_04::VALIDATION_RULE{ecl}->( 'asd' ), "Any other colour is invalid" );

        ok( $Day_04::VALIDATION_RULE{pid}->( '012345678' ), "A passport id with nine digits is valid" );
        ok( ! $Day_04::VALIDATION_RULE{pid}->( '01234567' ), "A passport id with less than nine digits is invalid" );
        ok( ! $Day_04::VALIDATION_RULE{pid}->( '0123456789' ), "A passport id with more than nine digits is invalid" );
        ok( ! $Day_04::VALIDATION_RULE{pid}->( 'PASSPORT1' ), "A passport id with non-digits is invalid" );
    };

    subtest 'Valid Records' => sub {
        my @validRecords = (
            {  byr => '1980',               ecl => 'grn',  eyr => '2030',  hcl => '#623a2f',  hgt => '74in',   iyr => '2012',  pid => '087499704' }, 
            {  byr => '1989', cid => '129', ecl => 'blu',  eyr => '2029',  hcl => '#a97842',  hgt => '165cm',  iyr => '2014',  pid => '896056539' }, 
            {  byr => '2001', cid => '88',  ecl => 'hzl',  eyr => '2022',  hcl => '#888785',  hgt => '164cm',  iyr => '2015',  pid => '545766238' }, 
            {  byr => '1944',               ecl => 'blu',  eyr => '2021',  hcl => '#b6652a',  hgt => '158cm',  iyr => '2010',  pid => '093154719' }, 
        );

        foreach my $record (@validRecords) {
            ok( validRecord( $record ), "Valid record should pass validation" );
        }
        
        my @invalidRecords = (
            { byr => '1926',  cid => '100',  ecl => 'amb',  eyr => '1972',  hcl => '#18171d',  hgt => '170',    iyr => '2018',  pid => '186cm' }, 
            { byr => '1946',                 ecl => 'grn',  eyr => '1967',  hcl => '#602927',  hgt => '170cm',  iyr => '2019',  pid => '012533040' }, 
            { byr => '1992',  cid => '277',  ecl => 'brn',  eyr => '2020',  hcl => 'dab227',   hgt => '182cm',  iyr => '2012',  pid => '021572410' }, 
            { byr => '2007',                 ecl => 'zzz',  eyr => '2038',  hcl => '74454a',   hgt => '59cm',   iyr => '2023',  pid => '3556412378' }, 
        );

        foreach my $record (@invalidRecords) {
            ok ( ! validRecord( $record ), "Invalid record should fail validation" );
        }
    };
};

done_testing();
