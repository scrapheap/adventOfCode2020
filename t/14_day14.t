use Modern::Perl;

use Test::More;
use Test::Deep;

use Day_14 qw( mask initialiseMemory locationMask initialiseMemory2 );

use List::Util qw( sum );

subtest 'Part 1' => sub {
    my $mask = mask( 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X' );

    is( $mask->( 11 ),   73, "The masked value of 11 should be 73" );
    is( $mask->( 101 ), 101, "The masked value of 101 should be 101" );
    is( $mask->( 0 ),    64, "The masked value of 0 should be 64" );

    my @data = (
        'mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X',
        'mem[8] = 11',
        'mem[7] = 101',
        'mem[8] = 0',
    );

    my @memory = initialiseMemory( @data );

    cmp_deeply(
        \@memory,
        [ undef, undef, undef, undef, undef, undef, undef, 101, 64 ],
        "our memory should match what we expect"
    );
};


subtest 'Part 2' => sub {
    my $mask = locationMask( '000000000000000000000000000000X1001X' );

    cmp_deeply(
        [ $mask->( 42 ) ],
        bag( 26, 27, 58, 59 ),
        "Our location mask should return all the memory locations to be updated"
    );

    $mask = locationMask( '00000000000000000000000000000000X0XX' );

    cmp_deeply(
        [ $mask->( 26 ) ],
        bag( 16, 17, 18, 19, 24, 25, 26, 27 ),
        "Our location mask should return all the memory locations to be updated"
    );

    my @data = (
        'mask = 000000000000000000000000000000X1001X',
        'mem[42] = 100',
        'mask = 00000000000000000000000000000000X0XX',
        'mem[26] = 1',
    );

    my %memory = initialiseMemory2( @data );
        
    is( sum( values %memory ), 208, "Our memory values should add up to 208 for this test" );
};


done_testing();
