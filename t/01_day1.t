use Modern::Perl;

use Test::More;
use Test::Deep;

use Day_01 qw( findTwoEntriesThatSumTo findThreeEntriesThatSumTo );

my @testExpenses = (
    1721,
    979,
    366,
    299,
    675,
    1456,
);

subtest 'Part 1' => sub {
    cmp_deeply(
        [ findTwoEntriesThatSumTo(2020, @testExpenses ) ],
        bag( 1721, 299 ),
        "Should find the two entries that sum to 2020"
    );
};

subtest 'Part 2' => sub {
    cmp_deeply(
        [ findThreeEntriesThatSumTo( 2020, @testExpenses ) ],
        bag( 979, 366, 675 ),
        "Should find the three entries that sum to 2020"
    );
};

done_testing();
