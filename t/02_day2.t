use Modern::Perl;

use Test::More;

use Day_02 qw( validatePasswordAgainstPolicy validatePasswordAgainstTobogganPolicy );

subtest 'Part 1' => sub {
    ok( validatePasswordAgainstPolicy( '1-3 a', 'abcde' ), "Valid passwords should pass" );
    ok( ! validatePasswordAgainstPolicy( '1-3 b', 'cdefg' ), "Invalid passwords should fail (no matching characters)");
    ok( validatePasswordAgainstPolicy( '2-9 c', 'ccccccccc' ), "Valid passwords should pass" );
    ok( ! validatePasswordAgainstPolicy( '2-9 c', 'caaaaaaaa' ), "Invalid passwords should fail (too few matching characters)" );
    ok( ! validatePasswordAgainstPolicy( '2-9 c', 'cccccccccc' ), "Invalid passwords should fail (too many matching characters)" );
};

subtest 'Part 2' => sub {
    ok( validatePasswordAgainstTobogganPolicy( '1-3 a', 'abcde' ), "Valid passwords should pass" );
    ok( ! validatePasswordAgainstTobogganPolicy( '1-3 b', 'cdefg' ), "Invalid passwords should fail (neither character matches)");
    ok( ! validatePasswordAgainstTobogganPolicy( '2-9 c', 'ccccccccc' ), "Invalid passwords should pass (both characters match)" );
};

done_testing();
