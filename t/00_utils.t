use Modern::Perl;

use Test::More;
use Test::Deep;

use Utils qw( trimAll gridFromRows );

subtest 'trimAll' => sub {
    cmp_deeply(
        [ trimAll( "   asdf   \n", "fds gdsf   \n", "   fdsjjf  fds\r\n", "fdas\r\n\r\n" ) ],
        [ 'asdf', 'fds gdsf', 'fdsjjf  fds', 'fdas' ], 
        "whitespace at the start and end of lines should be timmed"
    );
};

subtest 'gridFromRows' => sub {
    cmp_deeply(
        [ gridFromRows(
            '########',
            '#......#',
            '#.#..#.#',
            '#......#',
            '########',
        ) ],
        [
            [ '#', '#', '#', '#', '#', '#', '#', '#' ],
            [ '#', '.', '.', '.', '.', '.', '.', '#' ],
            [ '#', '.', '#', '.', '.', '#', '.', '#' ],
            [ '#', '.', '.', '.', '.', '.', '.', '#' ],
            [ '#', '#', '#', '#', '#', '#', '#', '#' ],
        ],
        "each character should become a new cell in our grid"
    );
};

done_testing();
