use Modern::Perl;

use Test::More;
use Test::Deep;

use Day_09 qw( isNotSumOfTwoOfThePrevious rangesThatAddUpTo );

my @list = (
    35,  20,  15,  25,  47,  40,  62,  55,  65,  95,  102,  117,
    150, 182, 127, 219, 299, 277, 309, 576
);


subtest 'Part 1' => sub {
    cmp_deeply(
        [ isNotSumOfTwoOfThePrevious( 5, @list ) ],
        [ 127 ],
        "We should find that 127 is not the sum of two of the previous 5 numbers"
    );
};


subtest 'Part 2' => sub { 
    cmp_deeply(
        [ rangesThatAddUpTo( 127, @list ) ],
        [ [ 15, 25, 47, 40 ] ],
        "We should find the range that adds up to 127"
    );
};

done_testing();
