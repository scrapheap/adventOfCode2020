use Modern::Perl;

use Test::More;
use Test::Deep;

use Day_13 qw( nearestBus nextPickupIn startOfOrderedSequence );

subtest 'Part 1' => sub {
    my $time = 939;

    is( nextPickupIn( $time, 7) ,  6,  "nextPickupIn should tell us when the next pickup for a bus is" );

    my @schedule = ( 7, 13, 'x', 'x', 59, 'x', 31, 19 );
    is( nearestBus( $time, @schedule ),  59,  "nearestBus should find the next bus we can take" );
};


subtest 'Part 2' => sub {
    my %schedule = (
        3417       => [   17, 'x',  13,   19                  ],
        1068781    => [    7,  13, 'x',  'x', 59, 'x', 31, 19 ],
        754018     => [   67,   7,  59,   61                  ],
        779210     => [   67, 'x',   7,   59, 61              ],
        1261476    => [   67,   7, 'x',   59, 61              ],
        1202161486 => [ 1789,  37,  47, 1889                  ],
    );

    foreach my $sequenceStartsAt ( sort keys %schedule ) {
        is( startOfOrderedSequence( @{ $schedule{ $sequenceStartsAt } } ),  $sequenceStartsAt,  "startOfOrderedSequence should find the earliest point that the busses will turn up in order ($sequenceStartsAt)" );
    }
};


done_testing();
