use Modern::Perl;

use Test::More;
use Test::Deep;

use Day_10 qw( deviceJoltLevel joltingDifferences joltHistogram buildTree walkTree );

my @adapterList1 = ( 16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4 );

my @adapterList2 = (
    28, 33, 18, 42, 31, 14, 46, 20, 48, 47, 24, 23, 49, 45, 19, 38,
    39, 11,  1, 32, 25, 35,  8, 17,  7,  9,  4,  2, 34, 10, 3,
);


subtest 'Part 1' => sub {
    
    my $deviceJoltLevel = deviceJoltLevel( @adapterList1 );

    is( $deviceJoltLevel, 22, "our device jolt level should be 3 higher than our max adapter" );

    my @joltingDifferences = joltingDifferences( 0, @adapterList1, $deviceJoltLevel );

    cmp_deeply(
        \@joltingDifferences,
        [ 1, 3, 1, 1, 1, 3, 1, 1, 3, 1, 3, 3 ],
        "The difference in jolts should be what we expect"
    );

    my %joltHistogram = joltHistogram( @joltingDifferences );
    cmp_deeply(
        \%joltHistogram,
        { 1 => 7, 3 => 5 },
        "Our jolt histogram should match that which we expect"
    );

    cmp_deeply(
        { joltHistogram(  joltingDifferences( 0, @adapterList2,  deviceJoltLevel( @adapterList2 )  )  ) },
        { 1 => 22, 3 => 10 },
        "Our jolt histogram for our second test set should match what we're expecting"
    );
};


subtest 'Part 2' => sub { 
    my %tree = buildTree( 0, @adapterList1, deviceJoltLevel( @adapterList1 ) );
    cmp_deeply(
        \%tree,
        {
            22 => [ 19 ],
            19 => [ 16 ],
            16 => [ 15 ],
            15 => [ 12 ],
            12 => [ 11, 10 ],
            11 => [ 10 ],
            10 => [ 7 ],
             7 => [ 6, 5 ,4 ],
             6 => [ 5, 4 ],
             5 => [ 4 ],
             4 => [ 1 ],
             1 => [ 0 ],
        },
        "our tree should match what we expect"
    );

    is(
        walkTree( deviceJoltLevel( @adapterList1 ), %tree ),
        8,
        "There should be 8 different permutations for our first test set"
    );

    is(
        walkTree( deviceJoltLevel( @adapterList2 ), buildTree( 0, @adapterList2, deviceJoltLevel( @adapterList2 ) ) ),
        19208,
        "There should be 19208 different permutations for our second test set"
    );
    
};

done_testing();
