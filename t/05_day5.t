use Modern::Perl;

use Test::More;
use Test::Deep;

use Day_05 qw( seatIdFromBoardingPass missingIds );


subtest 'Part 1' => sub {
    is( seatIdFromBoardingPass( 'BFFFBBFRRR' ), 567, "Boarding Pass should map to a seat ID (1)" );
    is( seatIdFromBoardingPass( 'FFFBBBFRRR' ), 119, "Boarding Pass should map to a seat ID (2)" );
    is( seatIdFromBoardingPass( 'BBFFBBFRLL' ), 820, "Boarding Pass should map to a seat ID (3)" );
};


subtest 'Part 2' => sub {
    cmp_deeply(
        [ missingIds( 3, 4, 5, 7, 8, 9, 11, 13 ) ],
        [ 6, 10, 12 ],
        "All missing seat IDs should be found"
    );
};

done_testing();
