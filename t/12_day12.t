use Modern::Perl;

use Test::More;
use Test::Deep;

use Day_12::Ferry;
use Day_12::FerryWayPoint;

subtest 'Part 1' => sub {
    my @testRoute = (
        'F10' => { x => 10, y =>  0 },
        'N3'  => { x => 10, y =>  3 },
        'F7'  => { x => 17, y =>  3 },
        'R90' => { x => 17, y =>  3 },
        'F11' => { x => 17, y => -8 },
    );

    my $ferry = Day_12::Ferry->new();

    cmp_deeply(
        { $ferry->position() },
        { x => 0, y => 0 },
        "Before it moves our ferry should be at the start position"
    );

    while (@testRoute) {
        my $instruction = shift @testRoute;
        $ferry->move( $instruction );
        
        my $expectedPosition = shift @testRoute;
        cmp_deeply(
            { $ferry->position() },
            $expectedPosition,
            "Our move of $instruction should put us at $expectedPosition->{x}, $expectedPosition->{y}"
        );
    }
};


subtest 'Part 2' => sub {
    my @testRoute = (
        'F10' => { x => 100, y =>  10 },
        'N3'  => { x => 100, y =>  10 },
        'F7'  => { x => 170, y =>  38 },
        'R90' => { x => 170, y =>  38 },
        'F11' => { x => 214, y => -72 },
    );

    my $ferry = Day_12::FerryWayPoint->new();

    cmp_deeply(
        { $ferry->position() },
        { x=> 0, y => 0 },
        "Before it moves our ferry should be at the start position"
    );

    while (@testRoute) {
        my $instruction = shift @testRoute;
        $ferry->move( $instruction );
        
        my $expectedPosition = shift @testRoute;
        cmp_deeply(
            { $ferry->position() },
            $expectedPosition,
            "Our move of $instruction should put us at $expectedPosition->{x}, $expectedPosition->{y}"
        );
    }
};


done_testing();
